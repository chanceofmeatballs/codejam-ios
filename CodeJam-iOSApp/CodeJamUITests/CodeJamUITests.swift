//
//  CodeJamUITests.swift
//  CodeJamUITests
//
//  Created by Prakasam Nampoothiripad on 10/1/18.
//  Copyright © 2018 ProgCodeJam. All rights reserved.
//

import XCTest

class CodeJamUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTapLogin() {
        XCUIApplication().buttons["Login"].tap()
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    func testTapBackButton() {
        
        let app = XCUIApplication()
        //XCTAssert(<#T##expression: Bool##Bool#>)
        app.buttons["Login"].tap()
        app.buttons["Back"].tap()
        
    }
    
    
}
